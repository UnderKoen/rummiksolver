package nl.underkoen.rummikub;

import lombok.*;

import java.util.Random;
import java.util.UUID;

/**
 * Created by Under_Koen on 27/10/2019.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Getter
@Setter
public class Stone implements Comparable {
    private static Random random = new Random();

    private final long id = random.nextLong();

    private final int number;
    private final Color color;
    private boolean joker = false;

    public static Stone createJoker() {
        return new Stone(0, null, true);
    }

    @Override
    public String toString() {
        if (joker) return "J";
        return String.format("%s%s", number, color.toString());
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Stone) {
            Stone stone = (Stone) o;
            if (stone.number == number) return 1;
            return number - stone.number;
        }
        return -1;
    }
}
