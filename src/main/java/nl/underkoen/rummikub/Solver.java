package nl.underkoen.rummikub;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Under_Koen on 27/10/2019.
 */
@RequiredArgsConstructor
@Getter
public class Solver {
    private final Set<Stone> game;
    private final Set<Stone> board;

    public static void main(String[] args) {
        String state = "G1,G2,G3,G4,G5,G6,G7,G8," +
//                "B2,B3,B4,B5,B6," +
//                "B9,G9,R9," +
//                "G4,Z4,R4," +
//                "R1,G1,B1," +
//                "G5,R5,B5," +
//                "B7,R7,Z7," +
//                "B9,J,B11," +
//                "B11,B12,B13," +
//                "Z10,Z11,Z12," +
                "Z8,B8,G8,R8," +
                "Z2,Z3,Z4," +
                "R3,R4,R5,R6," +
                "R6,R7,R8," +
                "R10,Z10,G10," +
                "B12,R12,Z12," +
                "G2,R2,B2," +
                "B13,R13,Z13,G13";

        Set<Stone> game = new HashSet<>();
        for (String s : state.split(",")) {
            if (s.equalsIgnoreCase("j")) {
                game.add(Stone.createJoker());
            } else {
                Color color;
                char c = Character.toLowerCase(s.charAt(0));
                if (c == 'r') color = Color.RED;
                else if (c == 'b') color = Color.BLUE;
                else if (c == 'z') color = Color.BLACK;
                else color = Color.YELLOW;

                int i = Integer.parseInt(s.substring(1));
                game.add(new Stone(i, color));
            }
        }

        long time = System.currentTimeMillis();
        Solver solver = new Solver(game, Collections.emptySet());
        solver.checkGame()
                .forEach(System.out::println)
        ;
        long diff = System.currentTimeMillis() - time;
        solver.printConfig();
        System.out.printf("Time taken: %s", diff);
    }

    private Set<Stone> getSameStones(Set<Stone> stones, int number) {
        return stones.stream()
                .filter(stone -> stone.isJoker() || stone.getNumber() == number)
                .collect(Collectors.toSet());
    }

    private Set<Stone> getSameStones(Set<Stone> stones, Color color) {
        return stones.stream()
                .filter(stone -> stone.isJoker() || stone.getColor() == color)
                .collect(Collectors.toSet());
    }

    private Set<Set<Stone>> getSameColorPairs(Set<Stone> stones, Set<Stone> current) {
        Set<Set<Stone>> pairs = new HashSet<>();
        if (current.size() >= 3) {
            pairs.add(current);
        }
        stones:
        for (Stone stone : stones) {
            for (Stone s : current) {
                if (stone.getColor() == s.getColor()) continue stones;
            }
            Set<Stone> stones2 = new HashSet<>(stones);
            Set<Stone> current2 = new HashSet<>(current);
            stones2.remove(stone);
            current2.add(stone);
            Set<Set<Stone>> r = getSameColorPairs(stones2, current2);
            pairs.addAll(r);
        }
        return pairs;
    }

    private Set<Set<Stone>> getOrderdPairs(Set<Stone> stones, Set<Stone> current, int latest) {
        Set<Set<Stone>> pairs = new HashSet<>();
        if (current.size() >= 3) {
            pairs.add(current);
            if (current.size() >= 5) return pairs;
        }
        for (Stone stone : stones) {
            if (latest != 0 && !stone.isJoker() && stone.getNumber() != latest + 1) continue;
            Set<Stone> stones2 = new HashSet<>(stones);
            Set<Stone> current2 = new HashSet<>(current);
            stones2.remove(stone);
            current2.add(stone);
            Set<Set<Stone>> r = getOrderdPairs(stones2, current2, stone.getNumber());
            pairs.addAll(r);
        }
        return pairs;
    }

    private Set<Set<Stone>> getPossiblePairs(Set<Stone> stones) {
        Set<Set<Stone>> pairs = new HashSet<>();

        //find same number pair
        for (int i = 1; i <= 14; i++) {
            Set<Stone> same = getSameStones(stones, i);
            int size = same.size();
            if (size < 3) continue;

            pairs.addAll(getSameColorPairs(same, new HashSet<>()));
        }

        //find ordered pairs
        for (Color color : Color.values()) {
            Set<Stone> same = new TreeSet<>(getSameStones(stones, color));
            if (same.size() < 3) continue;

            pairs.addAll(getOrderdPairs(same, new HashSet<>(), 0));
        }

        return pairs;
    }

    public Map<Integer, Integer> indents = new HashMap<>();

    private Set<Set<Stone>> checkBoardLoop(Set<Stone> remain, Set<Stone> used, Set<Set<Stone>> pairs, Set<Set<Stone>> combos, int indent) {
        int times = indents.getOrDefault(indent, 0);
        indents.put(indent, times + 1);
        System.out.println(indents);
        if (remain.size() == 0) return combos;
        if (remain.stream().allMatch(Stone::isJoker)) {
            combos.add(remain);
            return combos;
        }
        if (pairs.isEmpty()) return null;
        pair:
        for (Set<Stone> pair : pairs) {
            for (Stone stone : pair) {
                if (used.contains(stone)) continue pair;
            }
            Set<Stone> used2 = new HashSet<>(used);
            Set<Stone> remain2 = new HashSet<>(remain);
            Set<Set<Stone>> combos2 = new HashSet<>(combos);
            combos2.add(pair);
            remain2.removeAll(pair);
            used2.addAll(pair);
            Set<Set<Stone>> r = checkBoardLoop(remain2, used2, pairs, combos2, indent + 1);
            if (r != null) return r;
        }
        return null;
    }

    private Map<Stone, Set<Set<Stone>>> generateMappedPairs(Set<Stone> stones, Set<Set<Stone>> pairs) {
        Map<Stone, Set<Set<Stone>>> map = new HashMap<>();
        for (Stone stone : stones) {
            Set<Set<Stone>> stonePairs = new HashSet<>();

            if (!INCLUDE_JOKER && stone.isJoker()) {
                map.put(stone, stonePairs);
                continue;
            }

            for (Set<Stone> pair : new HashSet<>(pairs)) {
                if (pair.contains(stone)) {
                    stonePairs.add(pair);
                    if (!DOUBLE) pairs.remove(pair);
                }
            }
            map.put(stone, stonePairs);
        }
        return map;
    }

    private Set<Set<Set<Stone>>> statesChecked = new HashSet<>();

    private Set<Set<Stone>> checkBoardMapped(Map<Stone, Set<Set<Stone>>> mappedPairs, Set<Set<Stone>> current, Set<Stone> used, Collection<Stone> remain, int indent) {
        if (STATES_CHECKED && !statesChecked.add(current)) return null;
        int times = indents.getOrDefault(indent, 0);
        indents.put(indent, times + 1);
        System.out.println(indents);

        if (remain.isEmpty()) {
            System.out.println(indents);
            return current;
        }
        for (Stone stone : remain) {
            if (used.contains(stone)) continue;
            pair:
            for (Set<Stone> pair : mappedPairs.get(stone)) {
                for (Stone s : pair) {
                    if (used.contains(s)) continue pair;
                }
                Set<Set<Stone>> current2 = new HashSet<>(current);
                current2.add(pair);
                Set<Stone> used2 = new HashSet<>(used);
                used2.addAll(pair);
                List<Stone> remain2 = new ArrayList<>(remain);
                remain2.removeAll(pair);
                Set<Set<Stone>> r = checkBoardMapped(mappedPairs, current2, used2, remain2, indent + 1);
                if (r != null) return r;
            }
        }
        return null;
    }

    private Set<Set<Set<Stone>>> combinePairs(Set<Set<Set<Stone>>> boards, Set<Set<Stone>> pairs) {
        if (pairs.isEmpty()) return boards;
        Set<Set<Set<Stone>>> boards2 = new HashSet<>();
        for (Set<Stone> pair : pairs) {
            Set<Set<Stone>> board = new HashSet<>();
            board.add(pair);
            boards2.add(board);
        }
        if (boards.isEmpty()) return boards2;
        return combineBoards(boards, boards2);
    }

    private Set<Set<Set<Stone>>> combineBoards(Set<Set<Set<Stone>>> boards, Set<Set<Set<Stone>>> boards2) {
        System.out.println(boards.size());
        if (boards2.isEmpty()) return boards;
        Set<Set<Set<Stone>>> r = new HashSet<>(boards);
        for (Set<Set<Stone>> boardPairs : boards) {
            Set<Stone> boardStones = new HashSet<>();
            for (Set<Stone> boardPair : boardPairs) {
                boardStones.addAll(boardPair);
            }
            pair:
            for (Set<Set<Stone>> board2 : boards2) {
                for (Set<Stone> pair : board2) {
                    for (Stone stone : pair) {
                        if (boardStones.contains(stone)) continue pair;
                    }
                }
                Set<Set<Stone>> board = new HashSet<>(boardPairs);
                board.addAll(board2);
                r.add(board);
            }
        }
        return r;
    }

    private Set<Set<Stone>> getBestBoard(Set<Set<Set<Stone>>> boards) {
        Map<Integer, Set<Set<Set<Stone>>>> indexedBoards = new HashMap<>();
        for (Set<Set<Stone>> board : boards) {
            Set<Stone> stones = new HashSet<>();
            for (Set<Stone> pair : board) {
                if (!stones.addAll(pair)) throw new IllegalArgumentException("Something went wrong here");
            }
            int index = stones.size();
            indexedBoards.putIfAbsent(index, new HashSet<>());
            indexedBoards.get(index).add(board);
        }
        int max = Collections.max(indexedBoards.keySet());
        for (Set<Set<Stone>> board : indexedBoards.get(max)) {
            System.out.printf("Got %s stones out of ", max);
            return board;
        }
        return null;
    }

    private static final boolean BRUTE = false;
    private static final boolean COMBINE = true;
    private static final boolean DOUBLE = true;
    private static final boolean AFTER_SORT_DOUBLE = false;
    private static final boolean INCLUDE_JOKER = true;
    private static final boolean STATES_CHECKED = true;
    private static final boolean SORT = true;
    private static final boolean SORT_ORDER = true;

    private void printConfig() {
        System.out.printf("Brute: %s, Combine: %s, Double: %s, After Sort Double: %s, " +
                        "Include Joker: %s, States Checked: %s, Sort: %s, Sort Order: %s\n",
                BRUTE,
                COMBINE,
                DOUBLE,
                AFTER_SORT_DOUBLE,
                INCLUDE_JOKER,
                STATES_CHECKED,
                SORT,
                SORT_ORDER
        );
    }

    private Set<Set<Stone>> checkBoard(Set<Stone> stones) {
        Set<Set<Stone>> pairs = getPossiblePairs(stones);
        if (BRUTE) {
            return checkBoardLoop(stones, new HashSet<>(), getPossiblePairs(stones), new HashSet<>(), 0);
        } else {
            Map<Stone, Set<Set<Stone>>> mappedPairs = generateMappedPairs(stones, pairs);
            List<Stone> sortedStones = new ArrayList<>(stones);
            if (SORT) {
                sortedStones.sort(Comparator.comparingInt(o -> mappedPairs.get(o).size() * ((SORT_ORDER) ? -1 : 1)));
            }
            if (!AFTER_SORT_DOUBLE) {
                for (Stone s : sortedStones) {
                    for (Stone stone : sortedStones.subList(sortedStones.indexOf(s) + 1, sortedStones.size())) {
                        Set<Set<Stone>> p = mappedPairs.get(stone);
                        p.removeAll(mappedPairs.get(s));
                    }
                }
                sortedStones.sort(Comparator.comparingInt(o -> mappedPairs.get(o).size() * ((SORT_ORDER) ? -1 : 1)));
            }
            if (COMBINE) {
                Set<Set<Set<Stone>>> boards = new HashSet<>();
                for (int i = 0; i < sortedStones.size(); i++) {
                    boards = combinePairs(boards, mappedPairs.get(sortedStones.get(i)));
                }
                Set<Set<Stone>> bestBoard = getBestBoard(boards);
                System.out.println(sortedStones.size());
                return bestBoard;
            } else {
                return checkBoardMapped(mappedPairs, new HashSet<>(), new HashSet<>(), sortedStones, 0);
            }
        }
    }

    public Set<Set<Stone>> checkGame() {
        return checkBoard(game);
    }
}
