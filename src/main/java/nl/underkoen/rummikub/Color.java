package nl.underkoen.rummikub;

import lombok.AllArgsConstructor;

/**
 * Created by Under_Koen on 27/10/2019.
 */
@AllArgsConstructor
public enum Color {
    RED("R"), YELLOW("G"), BLUE("B"), BLACK("Z");

    private String name;

    @Override
    public String toString() {
        return name;
    }
}
